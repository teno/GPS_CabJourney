package com.learnovate.gpscab.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import com.learnovate.gpscab.Route;
import com.learnovate.gpscab.utils.FileUtils;


public class RouteTest {

	@Test
	public void testCase_RouteTest() {
		Route route = new Route();
		
		// Read the data from the file
		List<String> gpsData = null;
		try {
			gpsData = FileUtils.readTextFileByLines("data");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		
		// Add the data to the route class
		String delims = "\\s+";
		Iterator<String> iter = gpsData.iterator();
		while (iter.hasNext()){
			String data = iter.next();
			String[] splitData = data.split(delims);
			double latitude = Double.parseDouble(splitData[0]);
			double longitude = Double.parseDouble(splitData[1]);
			long timeStamp = Long.parseLong(splitData[2]);
			
			assertNotNull("latitude is NULL", latitude);
			assertNotNull("longitude is NULL", longitude);
			assertNotNull("timeStamp is NULL", timeStamp);
			
			route.addNode(latitude, longitude, timeStamp);
		}
		
		// Print out the Route
		route.printRoute();
		
		// Clean the data
		route.cleanRoute();
		
		// Print the cleaned data to a terminal 
		route.printRoute();
		
		// Write the cleanData to file
		// File can be uploaded to http://www.gpsvisualizer.com/ to visualise the coords
		try {
			FileUtils.writeToTextFile("cleanData", route.toStringforFile(), StandardOpenOption.CREATE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

}
