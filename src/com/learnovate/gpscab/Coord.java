package com.learnovate.gpscab;

import com.learnovate.gpscab.utils.LoggingUtils;

public class Coord {

	private static String LOG_TAG = "Coord";
	protected double latitude;
	protected double longitude;
	protected long timestamp;
	
	public Coord() {
		
	}
	
	void setLatitude(double latitude)
	{
		this.latitude = latitude;

	}

	void setLongitude(double longitude)
	{
		this.longitude = longitude;

	}
	
	void setTimeStamp(long timestamp)
	{
		this.timestamp = timestamp;

	}
	
	double getLatitude()
	{
		return latitude;
	}

	double getLongitude()
	{
		return longitude;
	}
	
	long getTimeStamp()
	{
		return timestamp;
	}
	
    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str = "Latitude = " + latitude +"\n";
		str += "Longitude = " + longitude +"\n";
		str += "TimeStamp = " + timestamp +"\n";		
		
		return str;

	}
	
    public static Double calculateHaversineDistance(Coord coord1, Coord coord2) 
    {

        final int R = 6371; // Radious of the earth in km
        Double lat1 = coord1.getLatitude();
        Double lon1 = coord1.getLongitude();
        Double lat2 = coord2.getLatitude();
        Double lon2 = coord2.getLongitude();
        Double latDistance = toRad(lat2-lat1);
        Double lonDistance = toRad(lon2-lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double distance = R * c;
         
        
        return distance;
 
    }
     


	private static Double toRad(Double value) 
    {
        return value * Math.PI / 180;
    }
	
}
