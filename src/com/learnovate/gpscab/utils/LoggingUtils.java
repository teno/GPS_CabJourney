package com.learnovate.gpscab.utils;

import java.io.IOException;
import java.nio.file.StandardOpenOption;

public class LoggingUtils {

	static boolean debug = false; 
	static boolean fileout = true;

	public static void Log(String tag, String msg)
	{
		if (debug){
			System.out.println(tag +": " + msg);
		}
		
		if (fileout){
			try {
				FileUtils.writeToTextFile("debugfile", tag +": " + msg +"\n", StandardOpenOption.APPEND);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		}
	}
	

}
