package com.learnovate.gpscab;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.learnovate.gpscab.utils.LoggingUtils;

public class Route {

	private static String LOG_TAG = "Route";
	
	// TimeStamp 1326378718 => GMT: Thu, 12 Jan 2012 14:31:58 GMT
	// Average driving speed in London taken from
	// http://www.itv.com/news/london/update/2012-11-19/tfl-average-traffic-speeds-are-less-than-20-mph-in-london/
	private final double CENTRAL_LONDON_AVG_SPEED = 4.01; // m/sec
	private final double LONDON_AVG_SPEED = 8.9408; // m/sec
	
	protected List<Coord> coords;
	
	public Route() {
		// TODO Auto-generated constructor stub
		coords = new ArrayList<Coord>();		
	}
	
	public void addNode(double latitude, double longitude, long timeStamp)
	{
		Coord coord = new Coord();
		coord.setLatitude(latitude);
		coord.setLongitude(longitude);
		coord.setTimeStamp(timeStamp);
		coords.add(coord);
		
	}
	
	public void cleanRoute()
	{
		List<Coord> coordstoRemove = new ArrayList<Coord>();
		
		int routeNo = 0;
		for(ListIterator<Coord> iter = coords.listIterator(); iter.hasNext(); )
		{
			routeNo++;

			Coord coord1 = iter.next();
			
			int nextIndex = iter.nextIndex();
			// Check if at the end of the route
			if (nextIndex == coords.size())
				continue;
			
			Coord coord2 = coords.get(nextIndex);
			
			LoggingUtils.Log(LOG_TAG,"Coord1 \n" +coord1.toString());
			LoggingUtils.Log(LOG_TAG,"Coord2 \n" +coord2.toString());
			
			double len_Coord1to2 = Coord.calculateHaversineDistance(coord1, coord2);
			
	        LoggingUtils.Log(LOG_TAG,"The distance between Coord1 and Coord2 is " + len_Coord1to2 + "km");

	        // Calculate speed
	        long time = (coord2.getTimeStamp() - coord1.getTimeStamp()); // in  sec
	        LoggingUtils.Log(LOG_TAG,"The time between Coord1 and Coord2 is " + time + "sec");
	        
	        double speed = len_Coord1to2*1000/time;
	        LoggingUtils.Log(LOG_TAG,"The speed between Coord1 and Coord2 is " + speed + "m/sec");
	        
	        // If speed is greater than LONDON_AVG_SPEED then remove then coordinate
			if (speed > LONDON_AVG_SPEED)
			{
				LoggingUtils.Log(LOG_TAG,"Removing Coord "+ routeNo + " LAT = " +coord1.getLatitude() 
						+", LONG = "+coord1.getLongitude() +", TIMESTAMP = "+coord1.getTimeStamp());
				coordstoRemove.add(coord1);
			}
					
		}
		
		coords.removeAll(coordstoRemove);
	}
	
	
	
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str = "";
		
		for (int i = 0; i < coords.size(); i++ )
		{
			str += "Route " + (i+1) +"\n";
			str += coords.get(i).toString();
			str += "\n";
		}

		return str;
		
	}
	
	
	public String toStringforFile()
	{
		String str = "Latitude, Longitude, TimeStamp \n";
		for (Coord coord : coords){
			str += coord.getLatitude() +",";
			str += coord.getLongitude() +",";
			str += coord.getTimeStamp() +"\n";
		}
		return str;
	}
	
	public void printRoute()
	{
		LoggingUtils.Log(LOG_TAG, this.toString());
	}
	
	


}
